﻿module HW4
open System

// Problem 1
let listProduct = 
    List.fold (*) 1  // This was deceptively simple. I kept overthinking.
    ;;
// END of Problem 1

// Problem 2
let app ys zs =
    List.foldBack (fun x xs -> x::xs) ys zs  // I did not have the typo.
    ;;
// END of Problem 2

// Problem 3
let doubleList lst =
    List.foldBack (fun x xs -> x::xs) lst lst
    ;;
// END of Problem 3

// Problem 4
let backwardsAndForwardsList lst =
    List.fold (fun x xs -> xs::x) lst lst
    ;;
// END of Problem 4


// Problem 5
type Polynomial = (float * int) list
let evaluate (poly:Polynomial) (x:float) =
    List.fold  (fun sum (coef, exp)  -> sum + (coef * (x ** float exp))  )  0.0  poly
    ;;
// END of Problem 5

    
// Problem 6
let dom inputSet =
    Set.fold (fun acc tup -> Set.add (fst(tup)) acc) Set.empty inputSet
    ;;
// END of Problem 6


// Problem 7
let mapDom inputMap = 
    Map.fold (fun acc map _ -> Set.add map acc) Set.empty inputMap
    ;;
// END of Problem 7


// Problem 8
let sumOfRangeElts inputMap =
    Map.fold (fun acc key value -> acc + value)  0 inputMap
    ;;
// END of Problem 8

// Problem 9
let prefixKeysToValues inputMap=
    Map.map (fun key (value:int) -> (key+(value.ToString()))) inputMap
    ;;
// END of Problem 9

// Problem 10
let compose m1 m2 =
    (Map.empty, m1 ) ||> Map.fold (fun acc key1 value1 ->
        if(Map.containsKey value1 m2) then
         Map.add key1 (Map.find value1 m2) acc else
            acc)
    ;;
// END of Problem 10


